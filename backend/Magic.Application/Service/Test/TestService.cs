﻿using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Application.Service;

[ApiDescriptionSettings("Application",Name = "Test", Order = 1)]
public class TestService : ITestService, IDynamicApiController, ITransient
{
    private readonly SqlSugarRepository<Documentation> _userRep;

    public TestService(SqlSugarRepository<Documentation> userRep)
    {
        _userRep=userRep;
    }

    [HttpGet("/test/TestChangeDatabase")]
    [AllowAnonymous]
    public async Task TestChangeDatabase()
    {
        var user = await _userRep.ToListAsync();

        _userRep.Ado.BeginTran();
        _userRep.Ado.CommitTran();

    }
}
